public class Games
{final static java.util.Scanner scan = new java.util.Scanner(System.in);
    private String name;
    private String company;
    private double cost;
    public void print()
	{
		System.out.println(name+"\n"+ company+"\n"+cost);
	}
	//set methods
	public void setName(String newName){
		this.name = newName;
	}public void setCompany(String company){
		this.company = company;
	}
	public void setCost(double gameCost)
	{
		this.cost = gameCost;
	}
	//get methods
	public String getName()
	{
		return this.name;
	}
	public String getCompany()
	{
	 return this.company;	
	}
	public double getCost()
	{
		return this.cost;
	}
	//constructor
	public Games(String gName, String gCompany, double gCost)
	{
		this.name = gName;
        this.company = gCompany;
        this.cost = gCost;	
	}
}